import socket
import json
SERVER_IP = "127.0.0.1"
SERVER_PORT = 1521

CODE_SIZE = 1
JSON_SIZE = 4
JSON_BYTES = "ASCII"
ORDER = "big"


def login_data():
    print("Enter the username: ")
    username = input()
    print("Enter the password: ")
    password = input()

    login = {
        "username": username,
        "password": password
    }

    print(json.dumps(login))
    return json.dumps(login)


def signup_data():
    print("Enter the username: ")
    username = input()
    print("Enter the password: ")
    password = input()
    print("Enter the email: ")
    email = input()

    signup = {
        "username": username,
        "password": password,
        "email": email
    }

    return json.dumps(signup)


def build_buffer(code, j):
    return int(code).to_bytes(1, "little") + len(j).to_bytes(4, "little") + bytes(j, JSON_BYTES)


with socket.socket() as sock:
    print("test")
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    print("Enter 1 if u want to login and 2 for signup")
    option = input()
    while option != "1" and option != "2":
        option = input("type 1 or 2 only!")

    if option == "1":
        tem = build_buffer(1, login_data())
        print(tem)
        sock.sendall(tem)
    if option == "2":
        sock.sendall(build_buffer(2, signup_data()))

    final = sock.recv(1)
    code = int.from_bytes(final,"little") #1000 - code

    print(code)

    final = sock.recv(4)

    length = int.from_bytes(final,"little") #1000 - code

    print(length)
    final = sock.recv(length)
    print(final)
    obj = json.loads(final)

    if obj["status"] == 1:
       print("nice!");