﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using System.Text.Json.Serialization;
using Client.Connection;

namespace Client
{
    /// <summary>
    /// Interaction logic for Create_Room.xaml
    /// </summary>
    /// 
    
    public struct CreateGame
    {

        public string name;
        public int maxUsers; 
        public int questionCount;
        public int answerTimeout;
    }
    public partial class Create_Room : Page
    {
        private Communicator _communicator;
        public Create_Room(Communicator com)
        {

            InitializeComponent();
            _communicator = com;

        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            string r_name = RoomName.Text;
            string time = Time.Text;
            int num_of_p = Int32.Parse(Num_of_p.Text);

            RequestManager.CreateRoomRequest createRoom = new RequestManager.CreateRoomRequest();
            createRoom.adminUser = _communicator._client_name;
            createRoom.roomName = r_name;
            createRoom.maxUsers = num_of_p;
            createRoom.questionCount = 10; //deafult
            createRoom.answerTimeout = Int32.Parse(time);

            RequestInfo result = _communicator.sendAndRecive(createRoom.Make());
            ResponseManager.CreateRoomResponse lr = ResponseManager.CreateRoomResponse.Make(result.json);

            if (lr.status == 0)
            {
                //server sent error print the error
                MessageBox.Show("Error", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                this.NavigationService.Navigate(new InRoomPage(_communicator, lr.roomId, _communicator._client_name));
            }


           
           

            
            


        }
    }
}
