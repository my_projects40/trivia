﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using System.Text.Json.Serialization;
using Client.Connection;

namespace Client
{
    /// <summary>
    /// Interaction logic for Join_Room.xaml
    /// </summary>
    /// 

    
    public partial class Join_Room : Page
    {
        private Communicator _communicator;
        private List<int> _roomIds;
        private List<string> _roomAdmins;
        public Join_Room(Communicator com)
        {
           

            InitializeComponent();
            _communicator = com;
            _roomIds = new List<int>();
            _roomAdmins = new List<string>();

            RequestManager.GetRooms request = new RequestManager.GetRooms();

            RequestInfo result = _communicator.sendAndRecive(request.Make());

            ResponseManager.GetRoomsResponse lr = ResponseManager.GetRoomsResponse.Make(result.json);//get msg from server


            if (lr.status == 0)
            {
                //server sent error print the error
                MessageBox.Show("Error", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                
                List<ResponseManager.RoomData> lst = lr.rooms;

                foreach (var v in lst)
                {
                    string str = "id: " + v.id + "\nName: " + v.name + "\nMax players: " + v.maxPlayers + "\nnum Of Questions In Game: " + v.numOfQuestionsInGame + "\ntime Per Question: " + v.timePerQuestion + "\nIs Active: " + v.isActive;
                    Rooms.Items.Add(str);
                    _roomIds.Add(v.id);
                    _roomAdmins.Add(v.adminUser);

                   
                }
            }

        }

        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            int id = _roomIds[Rooms.SelectedIndex];
            string admin = _roomAdmins[Rooms.SelectedIndex];


            RequestManager.JoinRoomRequest request = new RequestManager.JoinRoomRequest();
            request.roomId = id;

            RequestInfo result = _communicator.sendAndRecive(request.Make());

            ResponseManager.JoinRoomResponse lr = ResponseManager.JoinRoomResponse.Make(result.json);

            if(lr.status == 0)
                MessageBox.Show("Cant join the room", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                this.NavigationService.Navigate(new InRoomPage(_communicator, id, admin));
        }

        private void headLine_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}

