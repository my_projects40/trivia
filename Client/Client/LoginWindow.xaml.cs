﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;

using System.Text.Json;
using System.Text.Json.Serialization;
using Client.Connection;

namespace Client
{
   
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Page
    {
        private Communicator _communicator;

        public LoginWindow(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
        }
      
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            
            RequestManager.LoginRequest loginRequest = new RequestManager.LoginRequest();
            loginRequest.username = UserName.Text;
            loginRequest.password = password.Password;

            RequestInfo result = _communicator.sendAndRecive(loginRequest.Make());
            ResponseManager.LoginResponse lr = ResponseManager.LoginResponse.Make(result.json);

            if (lr.status == 0)
            {
                //server sent error print the error
                MessageBox.Show("Error", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                _communicator._client_name = UserName.Text;
                this.NavigationService.Navigate(new MenuWindow(_communicator));
            }






        }
    }
}
