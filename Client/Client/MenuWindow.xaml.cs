﻿using Client.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Page
    {
        Communicator _communicator;
        public MenuWindow(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
        }

        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Statistics(_communicator));
        }

        private void Create_Room_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Create_Room(_communicator));
        }

        private void Join_Room_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Join_Room(_communicator));
        }

        private void log_Out_Click(object sender, RoutedEventArgs e)
        {
            

            RequestManager.SignOutRequest signOutRequest = new RequestManager.SignOutRequest();
 
            RequestInfo result = _communicator.sendAndRecive(signOutRequest.Make());
            ResponseManager.LogoutResponse lr = ResponseManager.LogoutResponse.Make(result.json);

            if(lr.status == 0) //server sent error
            {
                //print error to screen
                MessageBox.Show("Error", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                _communicator._client_name = "";
                this.NavigationService.Navigate(new Welcome(_communicator));
            }
            
        }
    }
}
