﻿using Client.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for HighScores.xaml
    /// </summary>
    public partial class HighScores : Page
    {
        Communicator _communicator;
        public HighScores(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
            //GetHigtScores
            RequestManager.GetHigtScores request = new RequestManager.GetHigtScores();

            RequestInfo result = _communicator.sendAndRecive(request.Make());

            ResponseManager.GetHighScoreResponse lr = ResponseManager.GetHighScoreResponse.Make(result.json);

            if (lr.status == 0)
            {
                //server sent error print the error 
                MessageBox.Show("Cant show", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                
                List<string> lst = lr.statistics;
                if (lst.Count >= 0 && lst.Count < 3)
                {
                    MessageBox.Show("Bad status guys", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    BestScores.Text = lst[0];
                    BestScores.Text += "First: " + lst[0] + "\nSecond: " + lst[1] + "\nThird: " + lst[2];
                }
               
            
            }
        }
    }
}
