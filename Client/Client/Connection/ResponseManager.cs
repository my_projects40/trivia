﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Connection
{
    public class ResponseManager
    {

		public struct RoomData
		{
			public string adminUser { get; set; }
			public int id { get; set; }
			public string name { get; set; }
			public int maxPlayers { get; set; }
			public int numOfQuestionsInGame { get; set; }
			public int timePerQuestion { get; set; }
			public int isActive { get; set; }
		};

		public struct LoginResponse
		{
			public static LoginResponse Make(string str)
            {
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<LoginResponse>(str);

			}
			public int status { get; set; }

			
		};

		public struct SignUpResponse
		{
			public int status { get; set; }
			public static SignUpResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<SignUpResponse>(str);

			}
		};

		public struct ErrorResponse
		{
			public string message { get; set; }
			public static ErrorResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<ErrorResponse>(str);

			}
		};

		//----------2.0.0---------//
		public struct LogoutResponse
		{

			public int status { get; set; }
			public static LogoutResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<LogoutResponse>(str);

			}
		};

		public struct GetRoomsResponse
		{
			public int status { get; set; }
			public List<RoomData> rooms { get; set; }
			public static GetRoomsResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<GetRoomsResponse>(str);

			}

		};

		public struct GetPlayersInRoomResponse
		{
			public List<string> players { get; set; }
			public static GetPlayersInRoomResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<GetPlayersInRoomResponse>(str);

			}
		};

		public struct JoinRoomResponse
		{
			public int status { get; set; }
			public static JoinRoomResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<JoinRoomResponse>(str);

			}
		};

		public struct CreateRoomResponse
		{
			public string roomAdmin { get; set; }
			public int status { get; set; }
			public int roomId { get; set; }

			public static CreateRoomResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<CreateRoomResponse>(str);

			}
		};

		public struct GetHighScoreResponse
		{
			public int status { get; set; }
			public List<string> statistics { get; set; }
			public static GetHighScoreResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<GetHighScoreResponse>(str);

			}
		};

		public struct GetPersonalStatusResponse
		{
			public int status { get; set; }
			public List<string> statistics { get; set; }

			public static GetPersonalStatusResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<GetPersonalStatusResponse>(str);

			}
		};

		//---------3.0.0----------//

		public struct CloseRoomResponse
		{
			public int status { get; set; }
			public static CloseRoomResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<CloseRoomResponse>(str);

			}
		};

		public struct StartGameResponse
		{
			public static StartGameResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<StartGameResponse>(str);

			}
			public int status { get; set; }
		};

		public struct GetRoomStateResponse
		{
			public int status { get; set; }
			public bool hasGameBegun { get; set; }
			public List<string> players { get; set; }
			public int questionCount { get; set; }
			public int answerTimeout { get; set; }

			public static GetRoomStateResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<GetRoomStateResponse>(str);

			}
		};

		public struct LeaveRoomResponse
		{
			public int status { get; set; }

			public static LeaveRoomResponse Make(string str)
			{
				JObject json = JObject.Parse(str);

				return JsonConvert.DeserializeObject<LeaveRoomResponse>(str);

			}
		};

	}
}
