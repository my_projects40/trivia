﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Client
{
    public class RequestManager
    {


		public struct LoginRequest
		{
			public string username { get; set; }
			public string password { get; set; }

			public RequestInfo Make()
            {
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 1;
				loginInfo.json = JsonSerializer.Serialize(this);

				return loginInfo;
			}
		};

		
		public struct SignupRequest
		{
			public string username { get; set; }
			public string password { get; set; }
			public string email { get; set; }

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 2;
				loginInfo.json = JsonSerializer.Serialize(this);

				return loginInfo;
			}
		};
		public struct SignOutRequest
		{
			
			public  RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 4;
				loginInfo.json = "";

				return loginInfo;
			}
		};
		public struct GetPersonalStatusRequest
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 7;
				loginInfo.json = "";

				return loginInfo;
			}
		};

		public struct GetHigtScores
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 8;
				loginInfo.json = "";

				return loginInfo;
			}
		};

		//rooms requests

		public struct GetPlayersInRoomRequest
		{
			public int roomId { get; set; }

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 6;
				loginInfo.json = JsonSerializer.Serialize(this);

				return loginInfo;
			}
		};



		public struct GetRooms
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 5;
				loginInfo.json = "";

				return loginInfo;
			}
		};

		public struct JoinRoomRequest
		{
			public int roomId { get; set; }

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 9;
				loginInfo.json = JsonSerializer.Serialize(this);

				return loginInfo;
			}
		};

		public struct CreateRoomRequest
		{
			public string adminUser { get; set; }
			public string roomName { get; set; }
			public int maxUsers { get; set; }
			public int questionCount { get; set; }
			public int answerTimeout { get; set; }

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 10;
				loginInfo.json = JsonSerializer.Serialize(this);

				return loginInfo;
			}
		};
		public struct CloseRoom
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 11;
				loginInfo.json = "";

				return loginInfo;
			}
		};
		public struct LeaveRoom
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 14;
				loginInfo.json = "";

				return loginInfo;
			}
		};

		public struct GetRoomStateRequest
		{

			public RequestInfo Make()
			{
				RequestInfo loginInfo = new RequestInfo();
				loginInfo.code = 13;
				loginInfo.json = "";

				return loginInfo;
			}
		};
	}
}
