﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;



namespace Client
{
    public struct RequestInfo
    {
        public int code;
        public string json;
    }
    public class Communicator
    {
        public string _client_name;

        private TcpClient _socket;
        private NetworkStream _clientStream;
        public Communicator()
        {
            this._socket = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1521);
            _socket.Connect(serverEndPoint);
            this._clientStream = _socket.GetStream();
            _client_name = "";
        }
       

        public RequestInfo sendAndRecive(RequestInfo r)
        {
            SendMessage(r);

            return ReciveMessage();

        }
        public void SendMessage(RequestInfo r)
        {
            List<byte> msg = new List<byte>();
            msg.Add((byte)r.code);
            msg.AddRange(BitConverter.GetBytes(r.json.Length));
            msg.AddRange(Encoding.ASCII.GetBytes(r.json));

            byte[] request = msg.ToArray();

            _clientStream.Write(request, 0, request.Length);
            _clientStream.Flush();

        }

        public RequestInfo ReciveMessage()
        {
            RequestInfo rf = new RequestInfo();

            byte[] buffer = new byte[1]; //get the code
            int bytesRead = _clientStream.Read(buffer, 0, buffer.Length);
            rf.code = buffer[0];

            buffer = new byte[4];
            bytesRead = _clientStream.Read(buffer, 0, buffer.Length); //get size of json
            int size = BitConverter.ToInt32(buffer, 0);

            buffer = new byte[size];
            bytesRead = _clientStream.Read(buffer, 0, buffer.Length);//get the json
            rf.json = Encoding.Default.GetString(buffer);

            return rf;
        }
    }
}
