﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using Client.Connection;


namespace Client
{
    
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>
    public partial class Welcome : Page
    {
        private Communicator _communicator;


        public Welcome()
        {
            InitializeComponent();
            _communicator = new Communicator();
        }

        public Welcome(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
        }

        private void Button_SignIn(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Sign_Up(_communicator));
        }

        private void Button_Login(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new LoginWindow(_communicator));
        }
    }
}
