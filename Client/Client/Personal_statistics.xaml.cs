﻿using Client.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    
    /// <summary>
    /// Interaction logic for Personal_statistics.xaml
    /// </summary>
    public partial class Personal_statistics : Page
    {
        Communicator _communicator;
        public Personal_statistics(Communicator com)
        {
            InitializeComponent();
            _communicator = com;

            RequestManager.GetPersonalStatusRequest request = new RequestManager.GetPersonalStatusRequest();
            
            RequestInfo result = _communicator.sendAndRecive(request.Make());

            ResponseManager.GetPersonalStatusResponse lr = ResponseManager.GetPersonalStatusResponse.Make(result.json);//get msg from server

            if (lr.status == 0)
            {
                //server sent error print the error 
                MessageBox.Show("Error", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                List<string> lst = lr.statistics;
                Status.Text += "Num Of Correct Answer: " + lst[0] + "\nNum Of Players Games: " + lst[1] + "\nNum Of Total Answer: " + lst[2] + "Player Average Time: " + lst[3];
            }

        }

    }
}
