﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using System.Text.Json.Serialization;
using Client.Connection;

namespace Client
{
    
    public partial class Sign_Up : Page
    {
        Communicator _communicator;
        public Sign_Up(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
        }
        public static byte[] Combine(byte[] first, byte[] second)
        {
            return first.Concat(second).ToArray();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            

            int id = 2; //signup status is 2
            RequestManager.SignupRequest signupRequest = new RequestManager.SignupRequest();
            signupRequest.username = UserName.Text;
            signupRequest.password = password.Password;
            signupRequest.email = Email.Text; ;

            RequestInfo result = _communicator.sendAndRecive(signupRequest.Make());
            ResponseManager.SignUpResponse lr = ResponseManager.SignUpResponse.Make(result.json);

            if (lr.status == 0)
            {
                //server sent error print the error
                MessageBox.Show("User already exists", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else//server sent OK
            {
                _communicator._client_name = UserName.Text;
                this.NavigationService.Navigate(new MenuWindow(_communicator));
            }


        }
    }
}
