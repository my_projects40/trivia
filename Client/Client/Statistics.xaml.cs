﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Client
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Page
    {
        Communicator _communicator;
        public Statistics(Communicator com)
        {
            InitializeComponent();
            _communicator = com;
        }

        private void personal_statistics_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Personal_statistics(_communicator));
        }

        private void table_of_records_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HighScores(_communicator));
        }
    }
}
