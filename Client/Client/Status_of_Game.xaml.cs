﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for Status_of_Game.xaml
    /// </summary>

    public struct AddDetails
    {
        public string name;
        public string time;
        public int num;
    }
    public partial class Status_of_Game : Page
    {
        public Status_of_Game(string name, string time, int num)
        { 
            InitializeComponent();
            AddDetails addDetails = new AddDetails();
           
            addDetails.name = name;
            Statistic.Text += "Name of the room: " + addDetails.name + "\n";

            addDetails.time = time;
            Statistic.Text += "Time for qustion:" + addDetails.time + "\n";

            addDetails.num = num;
            Statistic.Text += "Number of players:" + addDetails.num.ToString() + "\n";
            

        }
    }
}
