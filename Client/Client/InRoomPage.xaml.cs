﻿using Client.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;


namespace Client
{
    /// <summary>
    /// Interaction logic for InRoomPage.xaml
    /// </summary>
    public partial class InRoomPage : Page
    {
        private int _roomId;
        private Communicator _communicator;
        private bool _isAdmin;
        private string _admin;
        private bool continueThread;

        public InRoomPage(Communicator communicator, int roomId, string admin)
        { 
            InitializeComponent();
            _admin = admin;
            _roomId = roomId;
            _communicator = communicator;
            continueThread = true;

            Thread t1 = new Thread(new ThreadStart(Refresh));
            t1.Start();
        }

        private void Leave()
        {
            continueThread = false;

            if (!_isAdmin)
            {
                RequestManager.LeaveRoom request = new RequestManager.LeaveRoom();

                RequestInfo result = _communicator.sendAndRecive(request.Make());

                ResponseManager.LeaveRoomResponse lr = ResponseManager.LeaveRoomResponse.Make(result.json);
                if (lr.status == 0)
                {
                    MessageBox.Show("Cant leave the room", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    Dispatcher.Invoke(() => NavigationService.Navigate(new MenuWindow(_communicator)));
                }

            }
            else
            {
                RequestManager.CloseRoom request = new RequestManager.CloseRoom();

                RequestInfo result = _communicator.sendAndRecive(request.Make());

                ResponseManager.CloseRoomResponse lr = ResponseManager.CloseRoomResponse.Make(result.json);
                if (lr.status == 0)
                    MessageBox.Show("Cant close the room", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    this.NavigationService.Navigate(new MenuWindow(_communicator));
            }
        }


        private void LeaveRoom_Click(object sender, RoutedEventArgs e)
        {
            Leave();
        }

        private void Refresh()
        {
            while(continueThread)
            {
                Dispatcher.Invoke(() => listOfUsers.Items.Clear()); //doing something smart

                
                RequestManager.GetRoomStateRequest request = new RequestManager.GetRoomStateRequest();
                RequestInfo result = _communicator.sendAndRecive(request.Make());
                ResponseManager.GetRoomStateResponse lr = ResponseManager.GetRoomStateResponse.Make(result.json);

                for (int i = 0; i < lr.players.Count; i++)
                {
                    if (_admin == lr.players[i])
                        Dispatcher.Invoke(() => listOfUsers.Items.Add("Admin: " + _admin));
                    else
                        Dispatcher.Invoke(() => listOfUsers.Items.Add(lr.players[i]));
                }

                if (_admin == _communicator._client_name)
                    _isAdmin = true;
                else
                    _isAdmin = false;

                if (lr.status == 99)//the room is closed
                    Leave();


                Thread.Sleep(3000);
            }
        }

        private void startGame_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

