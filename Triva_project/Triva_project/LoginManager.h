#pragma once
#include "LoggedUser.h"
#include <iostream>
#include <vector>
#include "IDatabase.h"
#include "SqliteDatabase.h"

class LoginManager
{

public:

	LoginManager(IDatabase* db);

	void signup(std::string user_name, std::string password, std::string email);
	void login(std::string user_name, std::string password);
	void logout(std::string user_name);

private:
	IDatabase *m_database;
	std::vector<LoggedUser>	m_loggedUsers;
};

