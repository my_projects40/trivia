#include "MenuRequestHandler.h"





MenuRequestHandler::MenuRequestHandler(RoomManager& room, RequestHandlerFactory& Factory, StatisticManager& statisticsManager, const LoggedUser& m_user) : m_roomManager(room) , m_handlerFactory(Factory), m_statisticsManager(statisticsManager), m_user(m_user)
{
    
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo rf)
{
    return (rf.code == SignOut || rf.code == GetRooms || rf.code == GetPlayersInRoom || rf.code == GetPersonalStats || rf.code == GetHighScore || rf.code == JoinRoom || rf.code == CreateRoom);
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo rf)
{
    switch (rf.code)
    {
    case SignOut:

        return signout(rf);

        break;
    case GetRooms:

        return getRooms(rf);

        break;
    case GetPlayersInRoom:

        return getPlayersInRoom(rf);

        break;

    case GetPersonalStats:

        return getPersonalStats(rf);

        break;

    case GetHighScore:

        return getHighScore(rf);
        break;

    case JoinRoom:

        return joinRoom(rf);
        break;

    case CreateRoom:

        return createRoom(rf);
        break;


    }
}

RequestResult MenuRequestHandler::signout(RequestInfo rf)
{
    
    m_handlerFactory.getLoginManager().logout(m_user.getUsername());

    LogoutResponse response;
    response.status = OK;
    rf.buffer = JsonResponsePacketSerializer::serializeResponse(response);

    return RequestResult({ rf, m_handlerFactory.createLoginRequestHandler()});

}

RequestResult MenuRequestHandler::getRooms(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code;
    GetRoomsResponse rp = { OK, m_roomManager.GetRoomsData() };
    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(rp);
    rr.newHandler = this;

    return rr;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code; //setting the code
    GetPlayersInRoomResponse rp;
    
    GetPlayersInRoomRequest gt = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(rf.buffer);
    //after we have GetPlayersInRoomRequest we can get the room_Id inside to find the room, to return it's players
    rp.players = (m_roomManager.GetRooms()[gt.roomId]).getAllUsers();

    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(rp);//returning it to buffer
    rr.newHandler = this;

    return rr;
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code;
    
    GetPersonalStatusResponse gt = { OK , m_statisticsManager.getUserStatistics(m_user.getUsername()) };
    rr.newHandler = this;
    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(gt);

    return rr;
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code;

    
    GetHighScoreResponse rp = { OK, m_statisticsManager.getHightScore() };
    rr.newHandler = this;
    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(rp);
    
    return rr;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code;
    
    JoinRoomRequest jr = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(rf.buffer);
    
    (m_roomManager.GetRooms()[jr.roomId]).addUser(m_user);
    JoinRoomResponse rp;
    Room& room = m_roomManager.GetRooms()[jr.roomId];
    rr.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, room);

    rp.status = OK;
    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(rp);

    return rr;

}

RequestResult MenuRequestHandler::createRoom(RequestInfo rf)
{
    RequestResult rr;
    rr.response.code = rf.code;
    
    CreateRoomRequest ct = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(rf.buffer);
    RoomData rd = copyRoom(ct);
   
    int id = m_roomManager.CreateRoom(m_user, rd);
    CreateRoomResponse rp;
    rp.adminUser = ct.adminUser;
    rp.roomId = id;
    rp.status = OK;
    rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(rp);
    Room& room = m_roomManager.GetRooms()[id];


    rr.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, room);
    return rr;
}

RoomData MenuRequestHandler::copyRoom(CreateRoomRequest ct)
{
    RoomData rd;
    rd.adminUser = ct.adminUser;
    rd.isActive = true;
    rd.maxPlayers = ct.maxUsers;
    rd.name = ct.roomName;
    rd.numOfQuestionsInGame = ct.questionCount;
    rd.timePerQuestion = ct.answerTimeout;
    rd.id = m_roomManager.GetCount();
    rd.id++;

    return rd;
    
}
