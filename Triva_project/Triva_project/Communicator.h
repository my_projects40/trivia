#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"

#include <stdlib.h>
#include <fstream>

#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <map>

#define PORT 1521


class Communicator
{

public:

	Communicator(RequestHandlerFactory& factory);

	void startHandleRequests();

	~Communicator();

	int getPartFromSocket(const SOCKET sc, const int bytesNum);
	std::string getJasonPartFromSocket(const SOCKET sc, const int bytesNum);

private:

	void bindAndListen();
	void handleNewClient(SOCKET);

	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;

	IDatabase* m_dataBase;

	RequestHandlerFactory& m_handlerFactory;

};



