#pragma once
#include "JsonResponsePacketSerializer.h"
#include "Response.h"
#include <iostream>


#define MSG "ERROR"
#define STATUS "status"


/*
This function serializes the LoginResponse message.
input: LoginResponse log
output: string
*/
std::string JsonResponsePacketSerializer::serializeResponse(LoginResponse log)
{
	json j = {
		{STATUS, log.status}
	};

	return j.dump();
}

/*
This function serializes the SignUpResponse message.
input: SignUpResponse sgn
output: string
*/
std::string JsonResponsePacketSerializer::serializeResponse(SignUpResponse sgn)
{
	json j = {
		{STATUS, sgn.status}
	};

	return j.dump();
}

/*
This function serializes the error message.
input: ErrorResponse err
output: string
*/
std::string JsonResponsePacketSerializer::serializeResponse(ErrorResponse err)
{
	json j = {
		{MSG, err.message}
	};

	return j.dump();
}


//-----2.0.0------//

std::string JsonResponsePacketSerializer::serializeResponse(LogoutResponse err)
{
	json j = {
		{STATUS, err.status}
	};
		
	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse err)
{
	
	json j = {
		{STATUS, err.status},
		{"rooms", json::array()}

	};
	
	for (const auto& room : err.rooms)
	{
		
		json jj =
		{
			{"adminUser", room.adminUser},
			{"id", room.id},
			{"name", room.name},
			{"maxPlayers", room.maxPlayers},
			{"numOfQuestionsInGame", room.numOfQuestionsInGame},
			{"timePerQuestion", room.timePerQuestion},
			{"isActive", room.isActive}
		};
		j["rooms"].push_back(jj);
	}
	
	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse log)
{
	json j = {
		
		{"players", log.players}

	};
	

	return j.dump();

}

std::string JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse sgn)
{
	json j = {

		{STATUS, sgn.status}

	};

	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse err)
{
	json j = {

		{STATUS, err.status},
		{"roomId", err.roomId},
		{"adminUser", err.adminUser}

	};

	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse err)
{
	json j = {

		{STATUS, err.status},
		{"statistics", err.statistics}

	};

	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(GetPersonalStatusResponse st)
{
	json j = {

		{STATUS, st.status},
		{"statistics", st.statistics}

	};

	return j.dump();
}


//-------3.0.0-------// 

std::string JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse closeRoom)
{
	json j = {

		{STATUS, closeRoom.status}

	};

	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGame)
{
	json j = {

		{STATUS, startGame.status}

	};

	return j.dump();
}


std::string JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse getState)
{
	json j = {

		{STATUS, getState.status},
		{"hasGameBegun", getState.hasGameBegun},
		{"players" , getState.players},
		{"questionCount" , getState.questionCount},
		{"answerTimeout", getState.answerTimeout}

	};

	return j.dump();
}

std::string JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoom)
{
	json j = {

		{STATUS, leaveRoom.status}

	};

	return j.dump();


}