#pragma warning(disable : 4996)

#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "MenuRequestHandler.h"

#define LOGIN 1
#define SIGNUP 2
#define FAIL 0
#define OK 1


char* intToCharArray(int number)
{
	
	char len[sizeof(int)];
	strncpy(len, (char*)&number, sizeof(int)); //dont ask me what it does

	return len;
}

LoginRequestHandler::LoginRequestHandler(LoginManager& login, RequestHandlerFactory& handler) : m_loginManager(login), m_handlerFactory(handler)
{


}

bool LoginRequestHandler::isRequestRelevant(RequestInfo rf)
{

	return(rf.code == LOGIN || rf.code == SIGNUP);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo rf) 
{
	if (rf.code == LOGIN)
	{
		return login(rf);
		
	}
	if (rf.code == SIGNUP)
	{
		return signup(rf);
	}
}



RequestResult LoginRequestHandler::login(RequestInfo inf)
{

	LoginRequest lr = JsonRequestPacketDeserializer::deserializeLoginRequest(inf.buffer);
	
	LoginResponse rp;

	
	IRequestHandler* next;
	
	try
	{
		m_loginManager.login(lr.username, lr.password);
		rp.status = OK;
		
		next = m_handlerFactory.createMenuRequestHandler({lr.username});
	}
	catch (const std::exception& e)
	{
		rp.status = FAIL;
		next = this;
	}

	std::string json = JsonResponsePacketSerializer::serializeResponse(rp);


	return { {inf.code,json}, next };
}


RequestResult LoginRequestHandler::signup(RequestInfo inf)
{
	SignupRequest sr = JsonRequestPacketDeserializer::deserializeSignupRequest(inf.buffer);

	SignUpResponse sp;
	IRequestHandler* next;

	try
	{
		m_loginManager.signup(sr.username, sr.password, sr.email);
		sp.status = OK;

		next = m_handlerFactory.createMenuRequestHandler({sr.username});

	}
	catch (const std::exception& e)
	{
		sp.status = FAIL;
		next = this;
	}
	
	std::string json = JsonResponsePacketSerializer::serializeResponse(sp);

	

	return { {inf.code,json}, next };
}
