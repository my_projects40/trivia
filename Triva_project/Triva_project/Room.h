#pragma once
#include "LoggedUser.h"
#include <vector>

#define EMPTY 9

struct RoomData
{
	std::string adminUser;
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive; //close or not close
};

class Room
{
public:
	Room() = default;

	Room(RoomData room);
	void addUser(LoggedUser logged);
	int removeUser(LoggedUser logged);
	RoomData getRoomData();
	std::vector<std::string> getAllUsers();
	void setStatus(int status);


private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;

};


