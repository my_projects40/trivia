#include "RoomManager.h"

#define ACTIVE 1

RoomManager::RoomManager()
{
	this->_count = 0;
}

int RoomManager::CreateRoom(LoggedUser user, RoomData data)
{
	data.id = _count++;
	Room rm(data);
	rm.addUser(user);
	m_rooms.insert({ data.id, rm });
	
	return data.id;
}

void RoomManager::DeleteRoom(int id)
{
	m_rooms.erase(id);
}

unsigned int RoomManager::GetRoomState(int id)
{
	return m_rooms[id].getRoomData().isActive;
}

std::vector<RoomData> RoomManager::GetRoomsData()
{
	std::vector<RoomData> rooms;

	for (std::map<unsigned int, Room>::iterator it = m_rooms.begin(); it != m_rooms.end(); ++it)
	{
		RoomData temp = it->second.getRoomData();
		
		if(temp.isActive == ACTIVE)
			rooms.push_back(temp);

		//it-->first gives you the key (int)
		//it->second gives you the mapped element (vector)
	}

	return rooms;
}

std::map<unsigned int, Room>& RoomManager::GetRooms()
{
	return m_rooms;
}

int RoomManager::GetCount()
{

	return _count;
}


