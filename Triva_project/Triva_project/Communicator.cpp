#pragma once

#include "Communicator.h"
#include "LoginRequestHandler.h"
#include <mutex>
#include <bitset> 

#define LOGIN 1
#define SIGNUP 2
#define BYTESOFSIZE 4 //the size of the jason in bytes

Communicator::Communicator(RequestHandlerFactory& factory) : m_handlerFactory(factory)
{
	
}

void Communicator::startHandleRequests()
{
	// the main thread is only accepting clients 
	// and add then to the list of handlers
	std::cout << "Waiting for client connection request" << std::endl;
	this->bindAndListen();

	while (true)
	{
	
		
		SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client accepted. Server and client can speak" << std::endl;
		// the function that handle the conversation with the client

		
		m_clients[client_socket] = m_handlerFactory.createLoginRequestHandler(); //adds to the map of users the new client.

		std::thread user_thread(&Communicator::handleNewClient, this, client_socket);
		user_thread.detach();

	}
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

int Communicator::getPartFromSocket(const SOCKET sc, const int bytesNum)
{
	char* data = new char[bytesNum];
	int res = recv(sc, data, bytesNum, 0);

	

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	//copies the char to string so I can delete the char after
	 //cleans the dynemic data
	int num;

	if(bytesNum == sizeof(char)) //1 - id
		num = (unsigned char)data[0];//converts from bytes to int
	else if(bytesNum == sizeof(int))// - char
		num = *((int*)data);

	delete[] data;

	return num;
}

std::string Communicator::getJasonPartFromSocket(const SOCKET sc, const int bytesNum)
{
	if (bytesNum == 0)
		return "";
	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = '\0';
	std::string cleanString(data);//copies the char to string so I can delete the char after
	delete[] data; //cleans the dynemic data
	

	return cleanString;
}

void Communicator::bindAndListen()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;


}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	
	while (true) 
	{
		try
		{
			int code = getPartFromSocket(clientSocket, 1); //1 represents first byte, if login 1 if 2 sign up and on

			int sizeOfJason = getPartFromSocket(clientSocket, BYTESOFSIZE);
			std::string json = getJasonPartFromSocket(clientSocket, sizeOfJason);

			std::cout << "code :" << code << "\n";
			std::cout << "json :" << json << "\n";

			RequestInfo request = { (RequestId)code, json };
		
			

			if (m_clients[clientSocket]->isRequestRelevant(request))
			{
				RequestResult result = m_clients[clientSocket]->handleRequest(request);//delete the memory if done good
				m_clients[clientSocket] = result.newHandler;

				char* temp = result.response.toCharArray();
				
				std::cout << result.response.buffer;

				
				std::cout << "\n";
				send(clientSocket, temp, result.response.size(), 0);
				delete[] temp;

			}
		}
		catch (const std::exception& e)
		{
			break; //disconnects the client.. :(
		}
		


		
	}


}
