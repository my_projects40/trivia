#pragma once
#include <iostream>
#include <vector>
class IRequestHandler;

enum RequestId {
	LoginIn = 1,
	SignUp,
	LogOut,
	SignOut,
	GetRooms,
	GetPlayersInRoom,
	GetPersonalStats,
	GetHighScore,
	JoinRoom,
	CreateRoom,
	CloseRoom,
	StartGame,
	GetRoomState,
	LeaveRoom

};

struct RequestInfo
{
	RequestId code;
	std::string buffer;

	char* toCharArray()	{
		char* arr = new char[size()];

		arr[0] = (unsigned char)code;

		int length = buffer.size();
		std::memcpy(arr + 1, &length, sizeof(int));

		std::memcpy(arr + 5, buffer.c_str(), buffer.size());
		return arr;
	};

	int size() {
		return sizeof(char) + sizeof(int) + buffer.size();
	}
};

struct RequestResult
{
	RequestInfo response;
	IRequestHandler* newHandler;
};


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo rf) = 0;
	virtual RequestResult handleRequest(RequestInfo rf) = 0;
};










	

