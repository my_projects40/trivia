#include "Room.h"

Room::Room(RoomData room)
{
    m_metadata = room;
}

void Room::addUser(LoggedUser logged)
{
    this->m_users.push_back(logged);

}

int Room::removeUser(LoggedUser logged)
{
    auto it = m_users.begin();
    for (; it != m_users.end(); it++)
    {
        if (it->getUsername() == logged.getUsername())
            break;
    }
    if (it == m_users.end())
        throw std::exception("User does not exists, can't erase\n");

    m_users.erase(it);
    if (m_users.empty())
        return EMPTY;

    return 0;
}

RoomData Room::getRoomData()
{
    return m_metadata;
}

std::vector<std::string> Room::getAllUsers()
{
    std::vector<std::string> users;

    for (auto it = m_users.begin(); it != m_users.end(); it++)
    {
        users.push_back(it->getUsername());
    }

    return users;
}

void Room::setStatus(int status)
{
    this->m_metadata.isActive = status;
}

