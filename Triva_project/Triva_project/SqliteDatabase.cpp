#include "SqliteDatabase.h"
#include "IDatabase.h"

#include <iostream>
#include <string> 
#include <io.h>
#include <algorithm>

#define SQLITE_OK  0 

//////////// CallBacks /////////////

int callBackGlobal(void* data, int argc, char** values, char** coloms)
{
	SqliteTable* table = (SqliteTable*)data;

	SqliteRow row;
	for (int i = 0; i < argc; i++)
		row.insert({ coloms[i], values[i] });

	table->push_back(row);

	return SQLITE_OK;
}

int LetItOutCallback(void* data, int argc, char** argv, char** azColName)
{
	Questions quest;
	std::vector<Questions>* exist = (std::vector<Questions>*)data;

	for (int i = 0; i < argc; i++)
	{
		if ((std::string(azColName[i]) == "QUESTION"))
			quest.question = argv[i];
		if ((std::string(azColName[i]) == "OPT1"))
			quest.result1 = argv[i];
		if ((std::string(azColName[i]) == "OPT2"))
			quest.result2 = argv[i];
		if ((std::string(azColName[i]) == "OPT3"))
			quest.result3 = argv[i];
		if ((std::string(azColName[i]) == "OPT4"))
			quest.result4 = argv[i];
		if ((std::string(azColName[i]) == "CORRECT"))
			quest.correct = argv[i];
				
	}

	exist->push_back(quest);
	
	return SQLITE_OK;
}


/////////// Tables /////////////////
SqliteDatabase::SqliteDatabase()
{
	this->createTable();
}

/*
* This function cretaes table of qustions
* input: none
* output: none
*/
int SqliteDatabase::createQustionsTable()
{
	char* errMsg = nullptr;
	int tbl_quest = sqlite3_exec(_db, "CREATE TABLE QUESTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QUESTION TEXT NOT NULL, OPT1 TEXT NOT NULL, OPT2 TEXT NOT NULL, OPT3 TEXT NOT NULL, OPT4 TEXT NOT NULL, CORRECT TEXT NOT NULL);", nullptr, nullptr, &errMsg);

	return  tbl_quest;
}

std::vector<Questions> SqliteDatabase::getQuestions(int num)
{
	std::vector<Questions> questions;
	char* errMsg = nullptr;
	std::string check = "SELECT * FROM QUESTIONS;";
	int res = sqlite3_exec(_db, check.c_str(), LetItOutCallback, &questions, &errMsg);

	return questions;
}

/*
* This function opens the database if it exist and if it not so just creats a new database
* input: none
* output: none
*/
void SqliteDatabase::createTable()
{
	char* errMsg = nullptr;
	std::string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);
	//if exist
	if (doesFileExist != 0) {

		char* errMsg = nullptr;
		int tbl_users = sqlite3_exec(_db, "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL);", nullptr, nullptr, &errMsg);
		int tbl_qst = this->createQustionsTable();
		if (tbl_users != SQLITE_OK && tbl_qst != SQLITE_OK)
			throw std::exception("Failed to create DB");
		else
			std::cout << "Records created Successfully!" << std::endl;
	

		//DB STATICTICS
		int tbl_stat = sqlite3_exec(_db, "CREATE TABLE STATISTICS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, AVERAGE_TIME REAL NOT NULL, CORRECT_ANSWER INTEGER NOT NULL, TOTAL_ANSWER INTEGER NOT NULL, NUM_OF_GAMES INTEGER NOT NULL, PLAYER_SCORE INTEGER NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY(USER_ID) REFERENCES STATISTICS(ID));", nullptr, nullptr, &errMsg);
		if (tbl_stat != SQLITE_OK)
			throw std::exception("Failed to create DB");
		std::cout << "Records created Successfully!" << std::endl;
		/// ?????????????????????????????????????????????????/
		int num_of_quest = 0;
		this->getQuestions(num_of_quest);

	}
	if (res != SQLITE_OK)
	{
		_db = nullptr;
		throw std::exception("Failed to open DB");
	}
}



////////// Finctions of tables ///////////////////
/*
* This function checks if user exist
* input: std::string user_name
* output: true/false
*/
bool SqliteDatabase::doesUserExist(std::string user_name)
{
	std::string sqlStatement = "SELECT * FROM USERS WHERE NAME = '" + user_name + "';";
	SqliteTable result;
	char* errMessage = nullptr;
	//existsCallback
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callBackGlobal, &result, &errMessage);
	if (res == SQLITE_OK  && !result.empty())
	{
		std::cerr << "User exists\n";
		
		return true;
	}
	std::cerr << "User doesnt exists\n";
	return false;
}

/*
* This function checks if user' password matches
* input: std::string user_name, std::string password
* output: true/false
*/
bool SqliteDatabase::doesPasswordMatch(std::string user_name, std::string password)
{
	char* errMsg = nullptr;
	SqliteTable result;

	std::string check = "SELECT * FROM USERS WHERE NAME = '" + user_name + "'AND PASSWORD = '" + password + "';";
	int rem = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);
	if (rem != SQLITE_OK || result.empty())
	{
		std::cerr << "The user' password doesnt match" << std::endl;
		sqlite3_free(errMsg);
		return false;
	}


	return true;
}

/*
* This function creates the User in database
* input: std::string user_name, std::string password, std::string email
* output: none
*/
void SqliteDatabase::addNewUser(std::string user_name, std::string password, std::string email)
{
	char* errMsg = nullptr;
	std::string us = "INSERT INTO USERS(ID, NAME, PASSWORD, EMAIL)VALUES(NULL,'" + user_name + "','" + password + "','" + email + "');";
	int rem = sqlite3_exec(_db, us.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot create the user" << std::endl;
		sqlite3_free(errMsg);
	}
	else
	{
		std::cout << "user created\n";
	}
	this->doesUserExist(user_name);

}

/*
* This function closes the connection with db
* input: none
* output: none
*/
void SqliteDatabase::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}



///////// Statistics ////////////

double SqliteDatabase::getPlayerAverageTime(std::string user_name)
{
	char* errMsg = nullptr;
	SqliteTable result;
	std::string check = "SELECT AVERAGE_TIME FROM STATISTICS WHERE USER_ID = '" + user_name + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);

	if (result.empty())
		return 0;

	return std::stod(result[0]["AVERAGE_TIME"]);
}
int SqliteDatabase::getNumOfCorrectAnswer(std::string user_name)
{
	SqliteTable result;
	char* errMsg = nullptr;
	std::string check = "SELECT CORRECT_ANSWER FROM STATISTICS WHERE USER_ID = '" + user_name + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);

	if (result.empty())
		return 0;

	return std::stod(result[0]["CORRECT_ANSWER"]);
}
int SqliteDatabase::getNumOfTotalAnswer(std::string user_name)
{
	SqliteTable result;
	char* errMsg = nullptr;
	std::string check = "SELECT TOTAL_ANSWER FROM STATISTICS WHERE USER_ID = '" + user_name + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);

	if (result.empty())
		return 0;

	return std::stod(result[0]["TOTAL_ANSWER"]);
}
int SqliteDatabase::getNumOfPlayersGames(std::string user_name)
{
	SqliteTable result;
	char* errMsg = nullptr;
	std::string check = "SELECT NUM_OF_GAMES FROM STATISTICS WHERE USER_ID = '" + user_name + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);

	if (result.empty())
		return 0;

	return std::stod(result[0]["NUM_OF_GAMES"]);
}

int SqliteDatabase::getPlayerScore(std::string user_name)
{
	SqliteTable result;
	char* errMsg = nullptr;
	std::string check = "SELECT PLAYER_SCORE FROM STATISTICS WHERE USER_ID = '" + user_name + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &result, &errMsg);

	if (result.empty())
		return 0;

	return std::stod(result[0]["PLAYER_SCORE"]);
}

std::map<std::string, int> SqliteDatabase::getTopUsers(int count)
{
	SqliteTable table;
	char* errMsg = nullptr;
	std::string check = "SELECT PLAYER_SCORE FROM STATISTICS ORDER BY PLAYER_SCORE DESC LIMIT '" + std::to_string(count) + "';";
	int res = sqlite3_exec(_db, check.c_str(), callBackGlobal, &table, &errMsg);

	std::map<std::string, int> result;
	for (SqliteRow row : table)
		result[row["ID"]] = std::stoi(row["PLAYER_SCORE"]);

	return result;
}
