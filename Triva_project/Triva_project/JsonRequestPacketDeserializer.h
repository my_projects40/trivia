#pragma once
#include "json.hpp"
#include "Request.h"

using json = nlohmann::json;

#include <iostream>
#include <string>


class JsonRequestPacketDeserializer
{
public:

	
	static LoginRequest deserializeLoginRequest(std::string buffer);
	static SignupRequest deserializeSignupRequest(std::string buffer);
	
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::string buffer); 
	static JoinRoomRequest deserializeJoinRoomRequest(std::string buffer); 
	static CreateRoomRequest deserializeCreateRoomRequest(std::string buffer);

	

};

