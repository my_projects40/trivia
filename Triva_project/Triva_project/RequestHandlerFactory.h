#pragma once

#include "LoginRequestHandler.h"
#include "LoginManager.h"
#include "MenuRequestHandler.h"
#include "RoomManager.h"
#include "StatisticManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"


class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory
{
public:

	RequestHandlerFactory(IDatabase* db);

	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);

	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	
	

	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user, Room& room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, Room& room);

private:
	LoginManager  m_loginManager;
	IDatabase* m_database;
	RoomManager m_roomManager;
	StatisticManager m_StatisticsManager;
};

