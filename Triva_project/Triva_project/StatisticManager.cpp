#include "StatisticManager.h"
#include "SqliteDatabase.h"
#include <vector>
#include <map>
#include <string>

std::vector<std::string> StatisticManager::getHightScore()
{
    int count = DEFAULT;
    SqliteDatabase sql;
    std::map<std::string, int> scores = sql.getTopUsers(count);
    std::vector<std::string> str;

    for (std::pair<std::string, int> element : scores) {
        std::string word = element.first;
        int count = element.second;
        std::string wow = word + " : " + std::to_string(count);
        str.push_back(wow);
    }

    return str;
}

std::vector<std::string> StatisticManager::getUserStatistics(std::string user_name)
{
    std::vector<std::string> stat;
    
    SqliteDatabase sql;

    int crAnsw = sql.getNumOfCorrectAnswer(user_name);
    int plGm = sql.getNumOfPlayersGames(user_name);
    int totAn = sql.getNumOfTotalAnswer(user_name);
    int plAvr = sql.getPlayerAverageTime(user_name);

    int arr[] = { crAnsw , plGm , totAn , plAvr };
    
    for (int i = 0; i < 4; i++)
        stat.push_back(std::to_string(arr[i]));

    return stat;
}
