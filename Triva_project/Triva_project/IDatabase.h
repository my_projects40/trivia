#pragma once
#include <iostream>
#include <map>
#include <iterator>
#include <vector>

struct Questions
{
	std::string question;
	std::string result1;
	std::string result2;
	std::string result3;
	std::string result4;
	std::string correct;
};
class IDatabase
{
public:
	virtual bool doesUserExist(std::string user_name) = 0;
	virtual bool doesPasswordMatch(std::string user_name, std::string password) = 0;
	virtual void addNewUser(std::string user_name, std::string password, std::string email) = 0;
	virtual std::vector<Questions> getQuestions(int num) = 0;
	//DB
	virtual double getPlayerAverageTime(std::string user_name) = 0;
	virtual int getNumOfCorrectAnswer(std::string user_name) = 0;
	virtual int getNumOfTotalAnswer(std::string user_name) = 0;
	virtual int getNumOfPlayersGames(std::string user_name) = 0;
	
};

