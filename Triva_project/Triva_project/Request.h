#pragma once
#include <iostream>

struct LoginRequest
{
	std::string username;
	std::string password;

};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

//rooms requests

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string adminUser;
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;

};

