#include "LoginManager.h"



LoginManager::LoginManager(IDatabase* db) : m_database(db)
{
	
	
}


void LoginManager::signup(std::string user_name, std::string password, std::string email)
{
	
	if (this->m_database->doesUserExist(user_name))
		throw std::exception("User already exists, can't create\n");

	this->m_database->addNewUser(user_name, password, email);
	
	m_loggedUsers.emplace_back(user_name);

}

void LoginManager::login(std::string user_name, std::string password)
{
	if (!this->m_database->doesUserExist(user_name)) //cant login to a user that doesnt exists in db
	{
		throw std::exception("User does not exists\n");
	}


	if (m_database->doesPasswordMatch(user_name, password))
	{
		LoggedUser lg(user_name);
		m_loggedUsers.push_back(lg);
	}
	else
	{
		throw std::exception("User already exists, can't create\n");
	}
}

void LoginManager::logout(std::string user_name)
{
	
	auto it = this->m_loggedUsers.begin();

	

	for (;it != this->m_loggedUsers.end(); it++)//
	{
		if (it->getUsername() == user_name)
		{
			break;
		}
	}
	if (it == m_loggedUsers.end())
	{
		throw std::exception("User does not exists, can't erase\n");
	}

	m_loggedUsers.erase(it);
}

