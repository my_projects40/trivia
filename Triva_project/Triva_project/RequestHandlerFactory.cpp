#include "RequestHandlerFactory.h"


RequestHandlerFactory::RequestHandlerFactory(IDatabase* db) : m_database(db), m_loginManager(db)
{
    
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
   
    return new MenuRequestHandler(m_roomManager, *this, m_StatisticsManager, user);
}



LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{

    return new LoginRequestHandler(m_loginManager, *this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    
    return m_loginManager;
}



RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room& id)
{
    return new RoomAdminRequestHandler(std::ref(id),user, m_roomManager, *this); 
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room& id)
{
    
    return new RoomMemberRequestHandler(std::ref(id),user, m_roomManager, *this);
}
