#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include <algorithm>
#include <stdlib.h>


#include "Communicator.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	void run();
	Server(IDatabase* db);

private:

	IDatabase* m_database;
	Communicator m_communicator;
	RequestHandlerFactory m_handlerFactory;

};



