#pragma once
#include <iostream>
#include "RoomManager.h"


struct LoginResponse
{
	unsigned int status;
};

struct SignUpResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};

//----------2.0.0---------//
struct LogoutResponse {

	unsigned int status;
};

struct GetRoomsResponse 
{
	unsigned int status;
	std::vector<RoomData> rooms;

};

struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	std::string adminUser;
	unsigned int status;
	unsigned int roomId;
};

struct GetHighScoreResponse 
{
	unsigned int status;
	std::vector<std::string> statistics;
};

struct GetPersonalStatusResponse
{
	unsigned int status;
	std::vector<std::string> statistics;

};

//---------3.0.0----------//

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status; 
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct LeaveRoomResponse
{
	unsigned int status;
};
