#pragma once
#include "JsonRequestPacketDeserializer.h"
using json = nlohmann::json;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::string buffer)
{
    json j = json::parse(buffer);


    return {j["username"], j["password"]};
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::string buffer)
{
    json j = json::parse(buffer);

    return { j["username"], j["password"], j["email"]};
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::string buffer)
{
    json j = json::parse(buffer);

    return { j["roomId"] };
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::string buffer)
{
    json j = json::parse(buffer);

    return { j["roomId"] };
}


CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::string buffer)
{
    json j = json::parse(buffer);

    return { j["adminUser"], j["roomName"], j["maxUsers"], j["questionCount"], j["answerTimeout"]};
}




