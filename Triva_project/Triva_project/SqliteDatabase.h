#pragma once
#pragma warning
#include "IDatabase.h"
#include "sqlite3.h"
#include <vector>
#include <map>

typedef std::map<std::string, std::string> SqliteRow;
typedef std::vector<SqliteRow> SqliteTable;


class SqliteDatabase : public IDatabase
{
public:
	
     SqliteDatabase();
	 void createTable();
	 int createQustionsTable();
	 

	 void close();
	 bool doesUserExist(std::string user_name) override;
	 bool doesPasswordMatch(std::string user_name, std::string password) override;
	 void addNewUser(std::string user_name, std::string password, std::string email) override;
	 std::vector<Questions> getQuestions(int num) override;
	 //Statistics
	 double getPlayerAverageTime(std::string user_name) override;
	 int getNumOfCorrectAnswer(std::string user_name) override;
	 int getNumOfTotalAnswer(std::string user_name) override;
	 int getNumOfPlayersGames(std::string user_name) override;
	 int getPlayerScore(std::string user_name);
	 std::map<std::string, int> getTopUsers(int count);


private:
	sqlite3* _db;
};

int existsCallback(void* data, int argc, char** values, char** coloms);

int callBackGlobal(void* data, int argc, char** values, char** coloms);
int LetItOutCallback(void* data, int argc, char** argv, char** azColName);