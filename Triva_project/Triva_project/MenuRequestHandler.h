#pragma once

#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "Request.h"
#include "StatisticManager.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

#define FAIL 0
#define OK 1


class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:

	MenuRequestHandler(RoomManager& room, RequestHandlerFactory& Factory, StatisticManager& statisticsManager, const LoggedUser& m_user);
	

	bool isRequestRelevant(RequestInfo rf) override;
	RequestResult handleRequest(RequestInfo rf) override;

	RequestResult signout(RequestInfo rf);
	RequestResult getRooms(RequestInfo rf);
	RequestResult getPlayersInRoom(RequestInfo rf);
	RequestResult getPersonalStats(RequestInfo rf);
	RequestResult getHighScore(RequestInfo rf);
	RequestResult joinRoom(RequestInfo rf);
	RequestResult createRoom(RequestInfo rf);
	
	RoomData copyRoom(CreateRoomRequest ct);
	
	
private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	StatisticManager& m_statisticsManager;
	RequestHandlerFactory& m_handlerFactory;


};

