#pragma once
#include <iostream>
#include "IDatabase.h"
#include <vector>
#define DEFAULT 3
class StatisticManager
{
public:

	std::vector<std::string> getHightScore();
	std::vector<std::string> getUserStatistics(std::string user_name);
private:
	IDatabase* m_database;
};

