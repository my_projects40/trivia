#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"

#define CLOSE 99
#define BEGUN 98

class RequestHandlerFactory;


class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(Room& room,LoggedUser user, RoomManager& manager, RequestHandlerFactory& handler);

	bool isRequestRelevant(RequestInfo request);
	RequestResult handleRequest(RequestInfo request);

private:
	Room& m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult startGame(RequestInfo request);
	RequestResult closeRoom(RequestInfo request);

	RequestResult getRoomState(RequestInfo request);
};
