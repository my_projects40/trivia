#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(Room& room, LoggedUser user ,RoomManager& manager, RequestHandlerFactory& handler) : m_room(room),m_roomManager(manager), m_handlerFactory(handler), m_user(user)
{
	
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo request)
{
	return (request.code == CloseRoom || request.code == StartGame || request.code == GetRoomState || request.code == LeaveRoom);
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo request)
{
	switch (request.code)
	{
	case CloseRoom:
		return closeRoom(request);
		break;

	case StartGame:
		return startGame(request);
		break;

	case GetRoomState:
		return getRoomState(request);
		break;
	}
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo request)
{
	
	m_room.setStatus(CLOSE);

	RequestResult RR;
	CloseRoomResponse closeRoomResponse;
	closeRoomResponse.status = OK;

	RR.response.code = CloseRoom;
	RR.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	RR.response.buffer = JsonResponsePacketSerializer::serializeResponse(closeRoomResponse);

	return RR;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo request)
{
	this->m_room.setStatus(CLOSE);

	RequestResult RR;
	StartGameResponse startGameResponse;
	startGameResponse.status = OK;

	RR.response.code = StartGame;
	RR.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	RR.response.buffer = JsonResponsePacketSerializer::serializeResponse(startGameResponse);

	return RR;
}

RequestResult RoomAdminRequestHandler::getRoomState (RequestInfo request)
{
	RequestResult rr;
	rr.newHandler = this;
	rr.response.code = GetRoomState;
	
	GetRoomStateResponse getState;
	getState.status = m_room.getRoomData().isActive;
	RoomData rd = m_room.getRoomData();
	
	getState.answerTimeout = rd.timePerQuestion;
	getState.players = m_room.getAllUsers();
	getState.questionCount = rd.numOfQuestionsInGame;
	getState.hasGameBegun = (rd.isActive == BEGUN); //If the code is begun 
	
	rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(getState);
	return rr;


}
