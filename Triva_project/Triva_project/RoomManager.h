#pragma once
#include <map>

#include "LoggedUser.h"
#include "Room.h"
#include <vector>




class RoomManager
{
public:

	RoomManager();
	int CreateRoom(LoggedUser user, RoomData data);
	void DeleteRoom(int id);
	unsigned int GetRoomState(int id);
	std::vector<RoomData> GetRoomsData();
	std::map<unsigned int, Room>& GetRooms();
	int GetCount();

private:
	int _count;
	std::map<unsigned int, Room> m_rooms;

};

