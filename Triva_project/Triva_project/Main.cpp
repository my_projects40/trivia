#pragma comment (lib, "ws2_32.lib")
#include "IDatabase.h"
#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <Set>

#define PORT 1521

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer(new SqliteDatabase());

		myServer.run(); //creates server(?)
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");

	return 0;
}
