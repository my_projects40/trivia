#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room& room, LoggedUser user, RoomManager& manager, RequestHandlerFactory& handler) : m_room(room),m_user(user), m_roomManager(manager), m_handlerFactory(handler)
{
	
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo request)
{
	return (request.code == GetRoomState || request.code == LeaveRoom);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo request)
{
	switch (request.code)
	{
	case LeaveRoom:
		return leaveRoom(request);
		break;

	case GetRoomState:
		return getRoomState(request);
		break;
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo request)
{
	
	RequestResult RR;
	LeaveRoomResponse leave;
	leave.status = OK;

	RR.response.code = LeaveRoom;
	RR.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	RR.response.buffer = JsonResponsePacketSerializer::serializeResponse(leave);
	int status = m_room.removeUser(m_user);
	if (status == EMPTY)
		m_roomManager.DeleteRoom(m_room.getRoomData().id);

	return RR;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo request)
{
	RequestResult rr;
	rr.newHandler = this;
	rr.response.code = GetRoomState;

	GetRoomStateResponse getState;
	getState.status = OK;
	RoomData rd = m_room.getRoomData();

	getState.status = rd.isActive;
	getState.answerTimeout = rd.timePerQuestion;
	getState.players = m_room.getAllUsers();
	getState.questionCount = rd.numOfQuestionsInGame;
	getState.hasGameBegun = (rd.isActive == BEGUN); //If the code is begun 

	rr.response.buffer = JsonResponsePacketSerializer::serializeResponse(getState);
	return rr;
}
