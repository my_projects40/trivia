#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "Response.h"
#include "Request.h"


class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:

	 LoginRequestHandler(LoginManager& m_Login, RequestHandlerFactory& m_handler);

	 bool isRequestRelevant(RequestInfo rf) override;
	 RequestResult handleRequest(RequestInfo rf) override;

private:

	
	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult login(RequestInfo inf);
	RequestResult signup(RequestInfo inf);

};