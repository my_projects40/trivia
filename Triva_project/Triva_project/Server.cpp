#pragma once

#include "Server.h"
#include "Communicator.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include "SqliteDatabase.h"


void Server::run()
{
	
	std::string input;
	

	std::thread t_connector(&Communicator::startHandleRequests, &m_communicator);
	t_connector.detach();

	try
	{
		while (true)
		{

			std::getline(std::cin, input);
			if (input == "EXIT")
				break;
		}
	}
	catch (...) {}

}



Server::Server(IDatabase* db) :m_database(db), m_handlerFactory(db), m_communicator(m_handlerFactory)
{
	
}
