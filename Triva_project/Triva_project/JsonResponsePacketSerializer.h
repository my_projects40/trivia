#pragma once
#include <iostream>
#include "Response.h"
#include "json.hpp"

using json = nlohmann::json;


class JsonResponsePacketSerializer
{
public:
	
	static std::string serializeResponse(LoginResponse log);
	static std::string serializeResponse(SignUpResponse sgn);
	static std::string serializeResponse(ErrorResponse err);

	//----2.0.0---//
	static std::string serializeResponse(LogoutResponse err);
	static std::string serializeResponse(GetRoomsResponse err); 
	static std::string serializeResponse(GetPlayersInRoomResponse log); 
	static std::string serializeResponse(JoinRoomResponse sgn); 
	static std::string serializeResponse(CreateRoomResponse err); 
	static std::string serializeResponse(GetHighScoreResponse err);
	static std::string serializeResponse(GetPersonalStatusResponse st);
	//----3.0.0---//

	static std::string serializeResponse(CloseRoomResponse closeRoom);
	static std::string serializeResponse(StartGameResponse startGame);
	static std::string serializeResponse(GetRoomStateResponse getState);
	static std::string serializeResponse(LeaveRoomResponse leaveRoom);


};

